package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/pdro.goncalves/omdbapi"
)

func readFile(file *string) ([]string, error) {

	f, err := os.Open(*file)
	if err != nil {
		return nil, err
	}
	fScanner := bufio.NewScanner(f)
	fScanner.Split(bufio.ScanLines)
	var fLines []string

	for fScanner.Scan() {
		fLines = append(fLines, fScanner.Text())
	}

	f.Close()

	return fLines, nil

}

func main() {

	apiKey := os.Getenv("OMDB_API_KEY")
	if apiKey == "" {
		fmt.Println("ERROR: OMDB_API_KEY not found. Set the environment variable OMDB_API_KEY")
		os.Exit(1)
	}

	file := flag.String("file", "movies.txt", "A path to the movies to collect file")
	output := flag.String("output", "movies_data.json", "A path to save collected data")
	poster := flag.Bool("poster", false, "A flag indicating to download or not the movies poster")

	flag.Parse()
	fmt.Println("Movies File:", *file)
	fmt.Println("Output File:", *output)
	fmt.Println("Download Poster:", *poster)

	api := omdbapi.NewClient(apiKey)

	movies, err := readFile(file)
	if err != nil {
		panic(err)
	}
	var movieData []*omdbapi.ImdbResponse

	for i, movie := range movies {
		fmt.Printf("%d - Quering for movie: %s\n", i, movie)
		// err = api.DownloadPoster(movie)
		data, err := api.SearchImdbByName(movie)
		if err != nil {
			fmt.Printf("Error getting data: %s\n", err)
		}
		movieData = append(movieData, data)
	}

	data, _ := json.MarshalIndent(movieData, "", " ")

	_ = ioutil.WriteFile(*output, data, 0644)

	if *poster {

		for i, movie := range movies {
			fmt.Printf("%d - Downloading do poster: %s\n", i, movie)
			err = api.DownloadPoster(movie)

			if err != nil {
				fmt.Printf("Error downloading poster: %s\n", err)
			}
		}
	}

}
