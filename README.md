(I)OMDB API Consumer
===============================

Overview
--------

A GO service that downloads movie data from [The Open Movie Database (OMDB) API](http://www.omdbapi.com/).


Installation
----------


Usage
--------------

Args:

- ```--file```: The path for a file containing the movies to be collected, one per line. (default: ```movies.txt```)
- ```--output```: The path where the json file containing the movies data will be saved. (default: ```movies_data.json```)
- ```--poster```: A flag indicating to download movie posters or not. (default: ```false```)


```shell
go run omdb.go --file movies.txt --output movies_data.json --poster
```
